# auto_register: false

require "dry/view/controller"
require "price_comparator/admin/container"

module PriceComparator
  module Admin
    module View
      class Controller < Dry::View::Controller
        configure do |config|
          config.paths = [Container.root.join("web/templates")]
          config.context = Container["view.context"]
          config.layout = "application"
        end
      end
    end
  end
end
