require "price_comparator/admin/view/controller"

module PriceComparator
  module Admin
    module Views
      class Welcome < View::Controller
        configure do |config|
          config.template = "welcome"
        end
      end
    end
  end
end
