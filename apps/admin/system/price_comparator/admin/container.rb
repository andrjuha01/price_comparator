require "pathname"
require "dry/web/container"
require "dry/system/components"

module PriceComparator
  module Admin
    class Container < Dry::Web::Container
      require root.join("system/price_comparator/container")
      import core: PriceComparator::Container

      configure do |config|
        config.root = Pathname(__FILE__).join("../../..").realpath.dirname.freeze
        config.logger = PriceComparator::Container[:logger]
        config.default_namespace = "price_comparator.admin"
        config.auto_register = %w[lib/price_comparator/admin]
      end

      load_paths! "lib"
    end
  end
end
