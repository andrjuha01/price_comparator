require "dry/web/roda/application"
require_relative "container"

module PriceComparator
  module Admin
    class Web < Dry::Web::Roda::Application
      configure do |config|
        config.container = Container
        config.routes = "web/routes".freeze
      end

      opts[:root] = Pathname(__FILE__).join("../../..").realpath.dirname

      use Rack::Session::Cookie, key: "price_comparator.admin.session", secret: self["core.settings"].session_secret

      plugin :dry_view
      plugin :error_handler
      plugin :flash
      plugin :multi_route

      route do |r|
        # Enable this after writing your first web/routes/ file
        # r.multi_route

        r.root do
          'hello'
        end

        r.is do
          r.get do
            r.view "welcome"
          end
        end
      end

      # Request-specific options for dry-view context object
      def view_context_options
        {
          flash:        flash,
        }
      end

      load_routes!
    end
  end
end
