require 'price_comparator/operation'
require 'bcrypt'

module PriceComparator
  module Main
    module Operations
      module Users
        class EncryptPassword < PriceComparator::Operation

          def call(user)
            password = user.delete(:password)
            email = user.delete(:email)
            account = { password_hash: BCrypt::Password.create(password), email: email }
            Right(account: account, user: user)
          end
        end
      end
    end
  end
end
