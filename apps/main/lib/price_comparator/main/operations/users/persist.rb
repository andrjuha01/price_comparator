require 'price_comparator/operation'

module PriceComparator
  module Main
    module Operations
      module Users
        class Persist < PriceComparator::Operation
          include Import['repositories.users_repo']

          def call(account:, user:)
            result = users_repo.create_with_account(user.merge(account: account))
            Right(result)
          end
        end
      end
    end
  end
end
