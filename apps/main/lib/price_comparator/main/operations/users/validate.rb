require 'price_comparator/operation'
require 'dry/validation'

module PriceComparator
  module Main
    module Operations
      module Users
        class Validate < PriceComparator::Operation
          EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i.freeze

          Schema = Dry::Validation.Form do
            configure do
              config.messages_file = 'lib/errors.yml'

              def unique_user?(attr_name, value)
                PriceComparator::Container['repositories.users_repo'].users.where(attr_name => value).to_a.empty?
              end

              def unique_account?(attr_name, value)
                PriceComparator::Container['repositories.accounts_repo'].accounts.where(attr_name => value).to_a.empty?
              end
            end

            required(:email).filled(format?: EMAIL_REGEX, unique_account?: :email)
            required(:password).filled(:str?, min_size?: 6)
            required(:name).filled(:str?, min_size?: 2, unique_user?: :name)
          end.freeze
          private_constant :Schema

          def call(*args)
            validation = Schema.(*args)

            if validation.success?
              Right(validation.output)
            else
              Left(validation)
            end
          end
        end
      end
    end
  end
end
