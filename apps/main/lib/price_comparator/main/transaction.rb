require 'dry/transaction'

module PriceComparator
  module Main
    class Transaction
      include Dry::Transaction(container: Container)
    end
  end
end
