require 'price_comparator/main/transaction'

module PriceComparator
  module Main
    module Transactions
      class Signup < Transaction
        step :validate,         with: 'operations.users.validate'
        step :encrypt_password, with: 'operations.users.encrypt_password'
        step :persist,          with: 'operations.users.persist'
      end
    end
  end
end

