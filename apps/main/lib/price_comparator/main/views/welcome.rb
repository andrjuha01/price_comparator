require "price_comparator/main/view/controller"

module PriceComparator
  module Main
    module Views
      class Welcome < View::Controller
        configure do |config|
          config.template = "welcome"
        end
      end
    end
  end
end
