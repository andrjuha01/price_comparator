require 'dry/web/roda/application'
require_relative 'container'

module PriceComparator
  module Main
    class Web < Dry::Web::Roda::Application
      configure do |config|
        config.container = Container
        config.routes = 'web/routes'.freeze
      end

      opts[:root] = Pathname(__FILE__).join('../../..').realpath.dirname

      use Rack::Session::Cookie, key: 'price_comparator.main.session', secret: self['core.settings'].session_secret

      plugin :json_parser
      plugin :symbol_status
      plugin :dry_view
      plugin :error_handler
      plugin :flash
      plugin :multi_route
      plugin :rodauth, json: :only do
        enable :login, :logout, :jwt
        require_bcrypt? true
        jwt_secret 'somesecretphrase'

        account_password_hash_column :password_hash
      end

      route do |r|
        # Enable this after writing your first web/routes/ file
        r.multi_route

        r.root do
          { text: 'Hello' }
        end
      end

      load_routes!
    end
  end
end
