require 'price_comparator/actions'

module PriceComparator
  module Main
    class Web
      include PriceComparator::Actions

      route 'users' do |r|
        r.rodauth

        r.post 'sign_up' do
          r.resolve 'transactions.signup' do |signup|
            signup.(r[:user]) do |m|
              m.success do |user|
                response['Content-Type'] = 'application/json'
                send_json(user, status: :created)
              end

              m.failure do |validation|
                response['Content-Type'] = 'application/json'
                send_json({ validation: validation.errors }.to_json, status: :unprocessable_entity)
              end
            end
          end
        end

        # require user
        rodauth.require_authentication

        r.get Integer do |id|
          "user with id #{id}"
        end
      end
    end
  end
end
