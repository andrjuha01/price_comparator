ROM::SQL.migration do
  change do
    alter_table(:users) do
      drop_column :password_digest
      drop_column :email
    end
  end
end
