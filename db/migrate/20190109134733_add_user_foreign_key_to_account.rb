ROM::SQL.migration do
  change do
    alter_table(:accounts) do
      add_foreign_key :user_id, :users
    end
  end
end
