ROM::SQL.migration do
  change do
    alter_table(:accounts) do
      add_column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      add_column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end
  end
end
