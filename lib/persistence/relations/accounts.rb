module Persistence
  module Relations
    class Accounts < ROM::Relation[:sql]
      schema(:accounts, infer: true) do
        associations do
          belongs_to :user
        end
      end
    end
  end
end
