# auto_register: false

module PriceComparator
  module Actions
    def send_json(body, status: 200)
      response.status = status
      response['Content-Type'] = 'application/json'

      body.to_json
    end
  end
end
