require 'price_comparator/repository'

module PriceComparator
  module Repositories
    class AccountsRepo < PriceComparator::Repository[:accounts]
      commands :create, update: :by_pk, delete: :by_pk

      struct_namespace PriceComparator::Entities
    end
  end
end
