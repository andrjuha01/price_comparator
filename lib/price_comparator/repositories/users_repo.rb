require 'price_comparator/repository'
require 'price_comparator/entities/user'

module PriceComparator
  module Repositories
    class UsersRepo < PriceComparator::Repository[:users]
      commands :create, update: :by_pk, delete: :by_pk

      struct_namespace PriceComparator::Entities

      def create_with_account(user)
        users.combine(:accounts).command(:create).call(user)
      end
    end
  end
end
