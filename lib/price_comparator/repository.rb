# auto_register: false

require "rom-repository"
require "price_comparator/container"
require "price_comparator/import"

module PriceComparator
  class Repository < ROM::Repository::Root
    include Import.args["persistence.rom"]
  end
end
