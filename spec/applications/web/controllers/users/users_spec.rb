require 'web_spec_helper'

RSpec.describe PriceComparator::Main::Web do
  include PriceComparator::Import['repositories.users_repo']

  describe 'POST #users/sign_up' do
    let(:headers) { { 'CONTENT-TYPE' => 'application/json' } }

    context 'when not valid arguments' do
      let(:create_user) { ->(user) { post 'users/sign_up', { user: user.to_h }, headers } }
      let(:invalid_user) { Factory.structs[:user, :invalid] }

      before do |test|
        create_user[invalid_user] unless test.metadata[:without_before]
      end

      it 'does not create user', :without_before do
        expect { create_user[invalid_user] }.not_to change(users_repo.users, :count)
      end

      it 'sets unprocessable status' do
        expect(last_response).to be_unprocessable
      end

      it 'returns validation errors' do
        expect(response['validation']).not_to be_empty
      end
    end

    context 'when valid arguments' do
      let(:user) { Factory.structs[:user].to_h.merge(password: 'Password') }
      let(:create_user) { ->(user) { post 'users/sign_up', { user: user }, headers } }

      before do |test|
        create_user[user] unless test.metadata[:without_before]
      end

      it 'creates user', :without_before do
        expect { create_user[user] }.to change(users_repo.users, :count).by(1)
      end

      it 'sets status :created' do
        expect(last_response).to be_created
      end

      it 'returns user' do
        expect(response).not_to be_empty
      end
    end
  end
end
