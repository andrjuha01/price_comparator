Factory.define :user do |f|
  f.sequence(:name) { |n| "UserName#{n}" }
  f.sequence(:email) { |n| "UserName#{n}@abc.com" }
  f.password_digest 'Hello2018!'
  f.timestamps

  f.trait :invalid do |i|
    i.name nil
    i.email nil
    i.password_digest nil
  end
end

