module Test
  module DatabaseHelpers
    module_function

    def rom
      PriceComparator::Container["persistence.rom"]
    end

    def db
      PriceComparator::Container["persistence.db"]
    end
  end
end
