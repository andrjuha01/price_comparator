module Test
  module WebHelpers
    module_function

    def app
      PriceComparator::Web.app
    end

    def response
      JSON.parse(last_response.body)
    end
  end
end
