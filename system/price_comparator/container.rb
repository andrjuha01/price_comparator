require "dry/web/container"
require "dry/system/components"

module PriceComparator
  class Container < Dry::Web::Container
    configure do
      config.name = :price_comparator
      config.listeners = true
      config.default_namespace = "price_comparator"
      config.auto_register = %w[lib/price_comparator]
    end

    load_paths! "lib"
  end
end
