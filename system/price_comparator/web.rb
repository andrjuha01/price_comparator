require 'dry/web/roda/application'
require_relative 'container'

module PriceComparator
  class Web < Dry::Web::Roda::Application
    configure do |config|
      config.container = Container
    end

    plugin :error_handler
    plugin :symbol_status
    plugin :json, classes: [Array, Hash, Struct, PriceComparator::Entities], serializer: proc { |object| object.to_json }
    plugin :all_verbs
    plugin :halt

    route do |r|
      r.on 'admin' do
        r.run Admin::Web.freeze.app
      end

      r.run Main::Web.freeze.app
    end

    error do |e|
      self.class[:rack_monitor].instrument(:error, exception: e)
      raise e
    end
  end
end
